export class RegisterUserDto {
  email: string;
  password: string;
  name: string;
  surname: string;
  phone: string;
  birthYear: number;
}
