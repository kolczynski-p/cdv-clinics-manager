export class RegisterDoctorDto {
  email: string;
  password: string;
  name: string;
  surname: string;
  phone: string;
  spec: string ;
  description: string;
  clinics: string;
}
