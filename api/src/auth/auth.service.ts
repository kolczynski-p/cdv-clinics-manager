import { forwardRef, Inject, Injectable,Res } from '@nestjs/common';
import { Doctor, Patient } from '@prisma/client';
import { filter } from 'rxjs';
import { Response } from 'express';
import { DoctorsService } from 'src/doctors/doctors.service';
import { PatientsService } from 'src/patients/patients.service';
import { PrismaService } from 'src/prisma/prisma.service';
import { hashPwd } from 'src/utils/hash-pass';
import { CreateAuthDto } from './dto/create-auth.dto';
import { RegisterUserResponseDto } from './dto/register-user-response.dto';
import { RegisterUserDto } from './dto/register-user.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { JwtPayload } from './jwt-strategy';
import { v4 as uuid } from 'uuid';
import {sign} from 'jsonwebtoken'
import { LoginDto } from './dto/login.dto';
import { LogoutDto } from './dto/logout.dto';
import { RegisterDoctorDto } from './dto/register-doctor.dto';


@Injectable()
export class AuthService {
  constructor(
    private db: PrismaService,
    @Inject(forwardRef(() => PatientsService))
    private readonly patientsService: PatientsService,
    @Inject(forwardRef(() => DoctorsService))
    private readonly doctorsService: PatientsService,
  ) {}

  async login(loginData: LoginDto, res: any): Promise<any> {
    let user : Patient | Doctor;
    console.log('enter login service')
    try {
      if (loginData.role === 'Patient') {
        user = await this.db.patient.findFirst({
          where: {
            email: loginData.email,
            passwordHash: hashPwd(loginData.password),
          },
        });
      } else if (loginData.role === 'Doctor') {
        console.log('fetching doctor');
        user = await this.db.doctor.findFirst({
          where: {
            email: loginData.email,
            passwordHash: hashPwd(loginData.password),
          },
        });
      }
      console.log(user);

      console.log('checking user')
      if (!user) {
        throw 'Invalid login data!' ;
      }
      const token = await this.createToken(await this.generateToken(user));
      console.log(token);
      return res
        .cookie('jwt', token.accessToken, {
          secure: false,
          domain: 'localhost',
          httpOnly: true,
        })
        .json({ ok: true, id:user.id });
    } catch (e) {
      console.log('error in login'+e);
      return res.status(400).json({ error: e });
    }
  }

  private createToken(currentTokenId: string): {
    

    
    accessToken: string;
    expiresIn: number;
  } {
    console.log('enter createToken');
    const payload: JwtPayload = { id: currentTokenId };
    const expiresIn = 60 * 60 * 24;
    const accessToken = sign(
      payload,
      'J*&SFDsdfj&f27FYSDHFBHSDb dhfBSHDfbSY #& 73ifuen  *83f2 bfh32bfudbskhf bsk fbw3 F9',
      { expiresIn },
    );
    console.log(accessToken+' '+expiresIn);
    return {
      accessToken,
      expiresIn,
    };
  }

  private async generateToken(user: Patient | Doctor): Promise<string> {
    console.log('enter generateToken');
    let token;
    let userWithThisToken = null;
    do {
      token = uuid();
      if (user.role === 'Patient') {
        userWithThisToken = await this.db.patient.findFirst({
          where: { currentTokenId: token },
        });
      } else if (user.role === 'Doctor') {
        userWithThisToken = await this.db.patient.findFirst({
          where: { currentTokenId: token },
        });
      }
    } while (!!userWithThisToken);

    if (user.role === 'Patient') {
      await this.db.patient.update({
        where: { id: user.id },
        data: { currentTokenId: token },
      });
    } else if (user.role === 'Doctor') {
      await this.db.doctor.update({
        where: { id: user.id },
        data: { currentTokenId: token },
      });
    }

    return token;
  }

  async register(newUser: RegisterUserDto): Promise<RegisterUserResponseDto> {
    const pwdHash = hashPwd(newUser.password);
    const createdUser = await this.db.patient.create({
      data: {
        email: newUser.email,
        passwordHash: pwdHash,
        name: newUser.name,
        surname: newUser.surname,
        phone: newUser.phone,
        birthYear: newUser.birthYear,
        isPaid: true,
        role: 'Patient',
       
        
      },
      
    });
    return this.filter(createdUser);
  }


  /////////////////
  async registerDoctor(
    newUser: RegisterDoctorDto,
  ): Promise<RegisterUserResponseDto> {
    const pwdHash = hashPwd(newUser.password);
    const createdUser = await this.db.doctor.create({
      data: {
        email: newUser.email,
        passwordHash: pwdHash,
        name: newUser.name,
        surname: newUser.surname,
        phone: newUser.phone,
        description: newUser.description,
        spec: newUser.spec,
        role: 'Doctor',
        clinics: {connect:[{id: newUser.clinics}]}
      },
    });
    console.log("created user:");
    console.log(createdUser);
    return this.filter(createdUser);
  }

  filter(user: Patient | Doctor) {
    console.log("enter filter");
    const { id, email, role } = user;
    console.log({id,email,role});
    return { id, email, role };
  }

  

  async logout(user: LogoutDto, res: any): Promise<any> {
    try {
      if (user.role === 'Patient') {
        await this.db.patient.update({
          where: { id: user.id },
          data: { currentTokenId: null },
        });
      } else if (user.role === 'Doctor') {
        await this.db.doctor.update({
          where: { id: user.id },
          data: { currentTokenId: null },
        });
      }

      res.clearCookie('jwt', {
        secure: false,
        httpOnly: true,
      });
      return res.json({ ok: true });
    } catch (e) {
      return res.json({ error: e.message });
    }
  }
}
