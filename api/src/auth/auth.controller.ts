import { Controller, Get, Post, Body, Patch, Param, Delete, Res, UseGuards,Response } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateAuthDto } from './dto/create-auth.dto';
import { LoginDto } from './dto/login.dto';
import { RegisterUserDto } from './dto/register-user.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { AuthGuard } from '@nestjs/passport';
import { LogoutDto } from './dto/logout.dto';
import { RegisterDoctorDto } from './dto/register-doctor.dto';
import { RegisterUserResponseDto } from './dto/register-user-response.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}




  @Post('/login')
  async login(@Body() req:LoginDto, @Res() res: Response): Promise<any>{
    return await this.authService.login(req, res);

  }

  @Post('/patient/register')
  async register(@Body() req:RegisterUserDto): Promise<RegisterUserResponseDto>{
    return await this.authService.register(req);

  }


  @Post('/doctor/register')
  async registerDoctor(@Body() req:RegisterDoctorDto, @Res() res: Response): Promise<any>{
    return await this.authService.registerDoctor(req);

  }

  @Post('/logout')
    @UseGuards(AuthGuard('jwt'))
    async logout( @Body() logoutDto: LogoutDto, @Res() res: Response) {
        return await this.authService.logout(logoutDto,res);
    }
}
