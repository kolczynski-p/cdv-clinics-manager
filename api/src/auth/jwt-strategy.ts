import { Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';


export interface JwtPayload {
    id: string;
}

function cookieExtractor(req: any): null | string {
    console.log("enter cookie extractor");
    return (req && req.cookies) ? (req.cookies?.jwt ?? null) : null;
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private db: PrismaService) {
        super({
            jwtFromRequest: cookieExtractor,
            secretOrKey: 'J*&SFDsdfj&f27FYSDHFBHSDb dhfBSHDfbSY #& 73ifuen  *83f2 bfh32bfudbskhf bsk fbw3 F9',
        });
    }
    
    async validate(payload: JwtPayload, done: (error, user) => void) {
        console.log('enter JwtStrategyValidate');
        console.log(payload);
        if (!payload || !payload.id) {

            return done(new UnauthorizedException(), false);
        }
        
        let role ='patient';
        let doctor;
        let user = await this.db.patient.findFirst({where:{ currentTokenId: payload.id }});
        if (!user) {
            console.log('!user error');
            doctor =  await this.db.doctor.findFirst({where:{ currentTokenId: payload.id }})
            if (!doctor) {
                console.log('!doctor error');
                return done(new UnauthorizedException(), false);
            }
            role= 'Doctor'
        }
        

        if (role ==='Doctor'){
            console.log('auth ok');
            done(null, doctor);
        }
        else{
            console.log('auth ok');
            done(null, user);
        }
    }
}


