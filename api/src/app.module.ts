import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClinicsModule } from './clinics/clinics.module';
import { PatientsModule } from './patients/patients.module';
import { DoctorsModule } from './doctors/doctors.module';
import { PrismaModule } from './prisma/prisma.module';
import { VisitsModule } from './visits/visits.module';
import { PatientsService } from './patients/patients.service';
import { VisitsService } from './visits/visits.service';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [ClinicsModule, PatientsModule, DoctorsModule, PrismaModule, VisitsModule, AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
