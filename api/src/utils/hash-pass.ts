import * as crypto from 'crypto';

export const hashPwd = (p: string): string => {
    const hmac = crypto.createHmac('sha512', 'asdaDWdwadawDAWfg61g61bfJFB hFAGsgf!1@4@3131j3fjbj BCJABcsbsbajsdbajbar312k');
    hmac.update(p);
    return hmac.digest('hex');
};
