import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  Inject,
  forwardRef,
} from '@nestjs/common';
import { DoctorsService } from 'src/doctors/doctors.service';
import { GetDoctorResponseDto } from 'src/doctors/dto/get-doctor-response.dto';
import { VisitsService } from 'src/visits/visits.service';
import { ClinicsService } from './clinics.service';
import { CreateClinicDto } from './dto/create-clinic.dto';
import { GetClinicResponseDto } from './dto/get-clinic-response.dto';
import { UpdateClinicDto } from './dto/update-clinic.dto';

@Controller('clinics')
export class ClinicsController {
  constructor(
    private readonly clinicsService: ClinicsService,
    
  ) {}

  //@Post()
  //async create(@Body() createClinicDto: CreateClinicDto): Promise<string> {
  //  await this.clinicsService.create(createClinicDto);
  //  return 'success';
  //}

  @Get()
  async findAll(): Promise<GetClinicResponseDto[]> {
    return await this.clinicsService.findAll();
  }
  @Get('/:id')
  async findOne(@Param('id') id: string): Promise<GetClinicResponseDto> {
    console.log("asdc");
    return await this.clinicsService.findOne(id);
  }

  @Get('/city/:city')
  async findAllInCity( @Param('city') city: string, ): Promise<GetClinicResponseDto[]> {
    const options = {
      where: {
        city: {
          equals: city,
        },
      },
    };
    return await this.clinicsService.findAll(options);
  }

  @Get('/doctors/:id')
  async findDoctors(@Param('id') id: string): Promise<GetClinicResponseDto> {
    console.log(id);
    return await this.clinicsService.findDoctors(id);
  }

 

  //@Patch(':id')
  //@HttpCode(204)
  //async update(@Param('id') id: string, @Body() updateClinicDto: UpdateClinicDto) {
  //  await this.clinicsService.update(id, updateClinicDto);
  //}
  //
  //@Delete(':id')
  //remove(@Param('id') id: string) {
  //  return this.clinicsService.remove(id);
  //}
}
