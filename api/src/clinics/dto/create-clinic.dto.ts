import { ApiProperty } from '@nestjs/swagger';
export class CreateClinicDto {
  name: string;
  address: string;
  city: string;
  picture?: Buffer;
}
