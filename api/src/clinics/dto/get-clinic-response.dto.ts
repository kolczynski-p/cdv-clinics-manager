import { PartialType } from '@nestjs/mapped-types';
import { CreateClinicDto } from './create-clinic.dto';
import { ApiProperty } from '@nestjs/swagger';

export class GetClinicResponseDto{
    id: string;
    name:string;
    address:string;
    city:string;
    latitude:string;
    longitude:string;
    picture?:string;
    createdAt:Date;



}
