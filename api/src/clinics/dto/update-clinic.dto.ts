import { PartialType } from '@nestjs/mapped-types';
import { CreateClinicDto } from './create-clinic.dto';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateClinicDto {

    id: string;
    name?:string;
    address?:string;
    city?:string;
    picture?:Buffer;

}
