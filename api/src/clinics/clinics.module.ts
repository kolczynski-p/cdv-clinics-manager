import { forwardRef, Module } from '@nestjs/common';
import { ClinicsService } from './clinics.service';
import { ClinicsController } from './clinics.controller';
import { DoctorsService } from 'src/doctors/doctors.service';
import { DoctorsModule } from 'src/doctors/doctors.module';

@Module({
  exports: [ClinicsService],
  imports: [forwardRef(() => DoctorsModule)],
  controllers: [ClinicsController],
  providers: [ClinicsService]
})
export class ClinicsModule {}
