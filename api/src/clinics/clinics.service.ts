import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { DoctorsService } from 'src/doctors/doctors.service';
import { GetDoctorResponseDto } from 'src/doctors/dto/get-doctor-response.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import HttpStatusCode from 'src/types/http-status-code';
import { CreateClinicDto } from './dto/create-clinic.dto';
import { GetClinicResponseDto } from './dto/get-clinic-response.dto';
import { UpdateClinicDto } from './dto/update-clinic.dto';

@Injectable()
export class ClinicsService {
  //inject global database module
  constructor(
    private db: PrismaService,
    @Inject(forwardRef(() => DoctorsService)) private readonly doctorsService: DoctorsService) {}

  //async create(createClinicDto: CreateClinicDto) {
  //  return 'This action adds a new clinic';
  //}

  async findAll(options?:any): Promise<GetClinicResponseDto[]> {
    const template = {
      orderBy: {
      city: 'asc',
    },
  };
    
    return await this.db.clinic.findMany({...template, ...options});
  }
  async findDoctors(id: string): Promise<GetClinicResponseDto> {
    const options = {
      where: {id: id},
      include:{doctors:true},
      };
    return await this.db.clinic.findFirst(options);
  }

  async findOne(id: string): Promise<GetClinicResponseDto> {
    return await this.db.clinic.findUnique({
      where: { id: id },
    });
  }

  //async update(id: string, updateClinicDto: UpdateClinicDto) {
  //  return  {};
  //}
//
  //async remove(id: string) {
  //  return `This action removes a #${id} clinic`;
  //}
}
