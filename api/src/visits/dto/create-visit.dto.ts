export class CreateVisitDto {
    startDate: string;
    endDate:string;
    doctorId:string;
    patientId:string;
    clinicId:string;

}
