import { Clinic, Doctor } from '@prisma/client';
import { GetClinicResponseDto } from 'src/clinics/dto/get-clinic-response.dto';
import { GetDoctorResponseDto } from 'src/doctors/dto/get-doctor-response.dto';
import { GetPatientResponseDto } from 'src/patients/dto/get-patient-response.dto';

export class GetVisitResponseDto {
  id: string;
  startDate: Date;
  endDate: Date;
  doctor?: GetDoctorResponseDto;
  clinic?: GetClinicResponseDto;
  patient?: GetPatientResponseDto;
}
