import { forwardRef, Module } from '@nestjs/common';
import { VisitsService } from './visits.service';
import { VisitsController } from './visits.controller';
import { DoctorsService } from 'src/doctors/doctors.service';
import { ClinicsService } from 'src/clinics/clinics.service';
import { PatientsService } from 'src/patients/patients.service';
import { PatientsModule } from 'src/patients/patients.module';
import { DoctorsModule } from 'src/doctors/doctors.module';
import { ClinicsModule } from 'src/clinics/clinics.module';

@Module({
  exports: [VisitsService],
  imports: [forwardRef(() => PatientsModule), forwardRef(() => DoctorsModule)],
  controllers: [VisitsController],
  providers: [VisitsService],
})
export class VisitsModule {}
