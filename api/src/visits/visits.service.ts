import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { Visit } from '@prisma/client';
import { DoctorsService } from 'src/doctors/doctors.service';
import { PatientsService } from 'src/patients/patients.service';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateVisitDto } from './dto/create-visit.dto';
import { GetVisitResponseDto } from './dto/get-visit-response.dto';
import { GetVisitsOptionsDto } from './dto/get-visits-options.dto';
import { UpdateVisitDto } from './dto/update-visit.dto';

@Injectable()
export class VisitsService {
  constructor(private db: PrismaService,
    @Inject(forwardRef(() => PatientsService)) private readonly patientsService:PatientsService,
    @Inject(forwardRef(() => DoctorsService)) private readonly doctorsService:PatientsService) {}

  async create(createVisitDto: CreateVisitDto): Promise<GetVisitResponseDto> {
    console.log('enter create visits');
    let startDate = new Date(createVisitDto.startDate);
    let endDate = new Date(createVisitDto.endDate);
    console.log(startDate)
    console.log(endDate)

    if (
      await this.checkDate(startDate, endDate, createVisitDto.doctorId)
    ) {
      console.log('date - ok');
      const newVisit = await this.db.visit.create({
        data: {
          startDate: startDate,
          endDate: endDate,
          doctorId: createVisitDto.doctorId,
          patientId: createVisitDto.patientId,
          clinicId: createVisitDto.clinicId,
        },
        
      });
      console.log(newVisit);
      return newVisit;
    } else {
      throw "Can't make an appointment, try again with different date";
    }
  }

  async findAll(options?:any):Promise<GetVisitResponseDto[]> {
    const template = {
      orderBy: {
      startDate: 'asc',
    },
  };

    return await this.db.visit.findMany({...template, ...options});
  }

  async findOne(id: string) :Promise<Visit>{
    return await this.db.visit.findUnique({where: {id: id}})
  }

  //update(id: string, updateVisitDto: UpdateVisitDto) {
  //  return `This action updates a #${id} visit`;
  //}
//
  //remove(id: number) {
  //  return `This action removes a #${id} visit`;
  //}

  async checkDate(startDate, endDate, doctorId)  {
    const visitDates = await this.db.visit.findMany({
      orderBy: {
        startDate: 'asc',
      },
      select: {
        startDate: true,
        endDate: true,
      },
      where: {
        doctor: { id: doctorId },
      },
    });
    console.log(visitDates);
    console.log('enter check date ');
    let isFree = true;
    const l = visitDates.length;
    if (l <= 0) {
      isFree = true;
    } else {
      console.log('else');
      for (var i = 0; i < l; i++) {
        if (
          (startDate>=visitDates[i].endDate	&&  endDate>visitDates[i].endDate) || (startDate<visitDates[i].startDate  &&  endDate<=visitDates[i].startDate)
          ) {
          console.log('continue');
          continue;
        }else{
          {
          
            console.log('free - false');
            isFree = false;
            break;
          }
        }
      }
    }

    return isFree;
  };
}
