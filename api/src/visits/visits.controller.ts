import { Controller, Get, Post, Body, Patch, Param, Delete, HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { VisitsService } from './visits.service';
import { CreateVisitDto } from './dto/create-visit.dto';
import { UpdateVisitDto } from './dto/update-visit.dto';
import { Visit } from '@prisma/client';
import { GetVisitResponseDto } from './dto/get-visit-response.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('visits')
export class VisitsController {
  constructor(private readonly visitsService: VisitsService) {}

  @UseGuards(AuthGuard('jwt'))
  @Post()
  async create(@Body() createVisitDto: CreateVisitDto) :Promise<GetVisitResponseDto> {
    try{
      return await this.visitsService.create(createVisitDto);

    }catch(error){
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  //@Get()
  //findAll() {
  //  return this.visitsService.findAll();
  //}

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<GetVisitResponseDto> {
    return await this.visitsService.findOne(id);
  }

  //@Patch(':id')
  //update(@Param('id') id: string, @Body() updateVisitDto: UpdateVisitDto) {
  //  return this.visitsService.update(id, updateVisitDto);
  //}
//
  //@Delete(':id')
  //remove(@Param('id') id: string) {
  //  return this.visitsService.remove(+id);
  //}
}
