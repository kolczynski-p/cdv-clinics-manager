/*
  Warnings:

  - You are about to alter the column `picture` on the `Clinic` table. The data in that column could be lost. The data in that column will be cast from `VarBinary(Max)` to `NVarChar(1000)`.
  - You are about to alter the column `picture` on the `Doctor` table. The data in that column could be lost. The data in that column will be cast from `VarBinary(Max)` to `NVarChar(1000)`.
  - You are about to drop the column `picture` on the `Patient` table. All the data in the column will be lost.

*/
BEGIN TRY

BEGIN TRAN;

-- AlterTable
ALTER TABLE [dbo].[Clinic] ALTER COLUMN [picture] NVARCHAR(1000) NULL;

-- AlterTable
ALTER TABLE [dbo].[Doctor] ALTER COLUMN [picture] NVARCHAR(1000) NULL;

-- AlterTable
ALTER TABLE [dbo].[Patient] DROP COLUMN [picture];

COMMIT TRAN;

END TRY
BEGIN CATCH

IF @@TRANCOUNT > 0
BEGIN
    ROLLBACK TRAN;
END;
THROW

END CATCH
