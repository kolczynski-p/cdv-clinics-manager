/*
  Warnings:

  - You are about to drop the column `password` on the `Doctor` table. All the data in the column will be lost.
  - You are about to drop the column `password` on the `Patient` table. All the data in the column will be lost.

*/
BEGIN TRY

BEGIN TRAN;

-- AlterTable
ALTER TABLE [dbo].[Doctor] DROP COLUMN [password];
ALTER TABLE [dbo].[Doctor] ADD [currentTokenId] NVARCHAR(1000),
[description] NVARCHAR(1000),
[passwordHash] NVARCHAR(1000),
[role] NVARCHAR(1000) NOT NULL CONSTRAINT [Doctor_role_df] DEFAULT 'Doctor';

-- AlterTable
ALTER TABLE [dbo].[Patient] DROP COLUMN [password];
ALTER TABLE [dbo].[Patient] ADD [currentTokenId] NVARCHAR(1000),
[passwordHash] NVARCHAR(1000),
[role] NVARCHAR(1000) NOT NULL CONSTRAINT [Patient_role_df] DEFAULT 'Patient';

COMMIT TRAN;

END TRY
BEGIN CATCH

IF @@TRANCOUNT > 0
BEGIN
    ROLLBACK TRAN;
END;
THROW

END CATCH
