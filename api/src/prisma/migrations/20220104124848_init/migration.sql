BEGIN TRY

BEGIN TRAN;

-- CreateTable
CREATE TABLE [dbo].[Clinic] (
    [id] NVARCHAR(1000) NOT NULL,
    [name] NVARCHAR(1000) NOT NULL,
    [address] NVARCHAR(1000) NOT NULL,
    [city] NVARCHAR(1000) NOT NULL,
    [picture] VARBINARY(max) NOT NULL,
    [createdAt] DATETIME2 NOT NULL CONSTRAINT [Clinic_createdAt_df] DEFAULT CURRENT_TIMESTAMP,
    [updatedAt] DATETIME2,
    CONSTRAINT [Clinic_pkey] PRIMARY KEY ([id])
);

-- CreateTable
CREATE TABLE [dbo].[Doctor] (
    [id] NVARCHAR(1000) NOT NULL,
    [email] NVARCHAR(1000) NOT NULL,
    [password] NVARCHAR(1000) NOT NULL,
    [name] NVARCHAR(1000) NOT NULL,
    [surname] NVARCHAR(1000) NOT NULL,
    [spec] NVARCHAR(1000) NOT NULL,
    [picture] VARBINARY(max) NOT NULL,
    [phone] NVARCHAR(1000) NOT NULL,
    [createdAt] DATETIME2 NOT NULL CONSTRAINT [Doctor_createdAt_df] DEFAULT CURRENT_TIMESTAMP,
    [updatedAt] DATETIME2,
    CONSTRAINT [Doctor_pkey] PRIMARY KEY ([id])
);

-- CreateTable
CREATE TABLE [dbo].[Patient] (
    [id] NVARCHAR(1000) NOT NULL,
    [email] NVARCHAR(1000) NOT NULL,
    [password] NVARCHAR(1000) NOT NULL,
    [name] NVARCHAR(1000) NOT NULL,
    [surname] NVARCHAR(1000) NOT NULL,
    [picture] VARBINARY(max) NOT NULL,
    [phone] NVARCHAR(1000) NOT NULL,
    [birthYear] INT NOT NULL,
    [createdAt] DATETIME2 NOT NULL CONSTRAINT [Patient_createdAt_df] DEFAULT CURRENT_TIMESTAMP,
    [updatedAt] DATETIME2,
    CONSTRAINT [Patient_pkey] PRIMARY KEY ([id])
);

-- CreateTable
CREATE TABLE [dbo].[Visit] (
    [id] NVARCHAR(1000) NOT NULL,
    [startDate] DATETIME2 NOT NULL,
    [isPaid] BIT NOT NULL CONSTRAINT [Visit_isPaid_df] DEFAULT 0,
    [createdAt] DATETIME2 NOT NULL CONSTRAINT [Visit_createdAt_df] DEFAULT CURRENT_TIMESTAMP,
    [updatedAt] DATETIME2,
    [doctorId] NVARCHAR(1000) NOT NULL,
    [patientId] NVARCHAR(1000) NOT NULL,
    [clinicId] NVARCHAR(1000) NOT NULL,
    CONSTRAINT [Visit_pkey] PRIMARY KEY ([id])
);

-- CreateTable
CREATE TABLE [dbo].[_ClinicToDoctor] (
    [A] NVARCHAR(1000) NOT NULL,
    [B] NVARCHAR(1000) NOT NULL,
    CONSTRAINT [_ClinicToDoctor_AB_unique] UNIQUE ([A],[B])
);

-- CreateIndex
CREATE INDEX [_ClinicToDoctor_B_index] ON [dbo].[_ClinicToDoctor]([B]);

-- AddForeignKey
ALTER TABLE [dbo].[Visit] ADD CONSTRAINT [Visit_doctorId_fkey] FOREIGN KEY ([doctorId]) REFERENCES [dbo].[Doctor]([id]) ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE [dbo].[Visit] ADD CONSTRAINT [Visit_patientId_fkey] FOREIGN KEY ([patientId]) REFERENCES [dbo].[Patient]([id]) ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE [dbo].[Visit] ADD CONSTRAINT [Visit_clinicId_fkey] FOREIGN KEY ([clinicId]) REFERENCES [dbo].[Clinic]([id]) ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE [dbo].[_ClinicToDoctor] ADD CONSTRAINT [FK___ClinicToDoctor__A] FOREIGN KEY ([A]) REFERENCES [dbo].[Clinic]([id]) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE [dbo].[_ClinicToDoctor] ADD CONSTRAINT [FK___ClinicToDoctor__B] FOREIGN KEY ([B]) REFERENCES [dbo].[Doctor]([id]) ON DELETE CASCADE ON UPDATE CASCADE;

COMMIT TRAN;

END TRY
BEGIN CATCH

IF @@TRANCOUNT > 0
BEGIN
    ROLLBACK TRAN;
END;
THROW

END CATCH
