/*
  Warnings:

  - You are about to drop the column `isPaid` on the `Visit` table. All the data in the column will be lost.
  - Added the required column `isPaid` to the `Patient` table without a default value. This is not possible if the table is not empty.
  - Added the required column `endDate` to the `Visit` table without a default value. This is not possible if the table is not empty.

*/
BEGIN TRY

BEGIN TRAN;

-- AlterTable
ALTER TABLE [dbo].[Patient] ADD [isPaid] BIT NOT NULL;

-- AlterTable
ALTER TABLE [dbo].[Visit] DROP CONSTRAINT Visit_isPaid_df;
ALTER TABLE [dbo].[Visit] DROP COLUMN [isPaid];
ALTER TABLE [dbo].[Visit] ADD [endDate] DATETIME2 NOT NULL;

COMMIT TRAN;

END TRY
BEGIN CATCH

IF @@TRANCOUNT > 0
BEGIN
    ROLLBACK TRAN;
END;
THROW

END CATCH
