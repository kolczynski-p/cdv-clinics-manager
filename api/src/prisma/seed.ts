import { PrismaClient } from '@prisma/client';
import { hashPwd } from '../utils/hash-pass';

const prisma = new PrismaClient();

async function main() {

//seed db with random'ish data
  const createClinic = await prisma.clinic.createMany({
    data: [
      { name: 'Hipokrates', address: 'Literacka 47A', city: 'Poznań',latitude: '52.447135', longitude: '16.8735603' , picture:'https://clinicsapp.blob.core.windows.net/pictures/clinics/clinic1.jpg'},
      { name: 'Ivita', address: 'Dąbrowskiego 77A', city: 'Poznań',latitude: '52.4139699', longitude: '16.9006447', picture:'https://clinicsapp.blob.core.windows.net/pictures/clinics/clinic2.jpg' },
      { name: 'MediPark', address: 'Obrzeżna 5', city: 'Warszawa', latitude: '52.1653584', longitude: '21.0001813', picture:'https://clinicsapp.blob.core.windows.net/pictures/clinics/clinic3.jpg' },
      { name: 'Prime', address: 'Topiel 12', city: 'Warszawa', latitude: '52.23905', longitude: '21.0247437' , picture:'https://clinicsapp.blob.core.windows.net/pictures/clinics/clinic4.jpg'},
      { name: 'CliniqMed', address: 'Zabłocie 25', city: 'Kraków', latitude: '50.0497556', longitude: '19.961283', picture:'https://clinicsapp.blob.core.windows.net/pictures/clinics/clinic5.jpg' },
    ],
  });

  const clinicsIds = await prisma.clinic.findMany({
      orderBy: {
      city: 'asc',
    },
  });

  console.log('clinicids:');
  console.log(clinicsIds);

  const createDoctor = async ()=>{
    console.log('seedingDoctors');
    const lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Suspendisse ultrices gravida dictum fusce ut. Neque egestas congue quisque egestas. Varius sit amet mattis vulputate enim nulla aliquet porttitor lacus. Odio facilisis mauris sit amet massa vitae tortor condimentum.'
    const pwdHash = hashPwd('password123');
    const doc1 = await prisma.doctor.create({
      data: 
        { name: 'Andrzej', surname: 'Wawrzyniak', passwordHash: pwdHash,email:'a.wawrzyniak@example.com',spec:'Kardiolog',phone:'123123123',description:lorem,role:'Doctor', picture:'https://clinicsapp.blob.core.windows.net/pictures/doctors/doctor1.jpg', clinics:{connect:[{id: clinicsIds[1].id},{id: clinicsIds[2].id}]}},
    });
    await prisma.doctor.create({
        data: 
        { name: 'Bartosz', surname: 'Kutrzeba', passwordHash: pwdHash,email:'b.kutrzeba@example.com',spec:'Internista',phone:'234234234',description:lorem,role:'Doctor', picture:'https://clinicsapp.blob.core.windows.net/pictures/doctors/doctor2.jpg', clinics:{connect:[{id: clinicsIds[1].id},{id: clinicsIds[2].id}]}},
    });
    await prisma.doctor.create({
        data: 
        { name: 'Marcin', surname: 'Kowalski', passwordHash: pwdHash,email:'m.kowalski@example.com',spec:'Internista',phone:'3456456456',description:lorem,role:'Doctor',picture:'https://clinicsapp.blob.core.windows.net/pictures/doctors/doctor3.jpg', clinics:{connect:[{id: clinicsIds[3].id},{id: clinicsIds[4].id}]}},
    });
    await prisma.doctor.create({
        data: 
        { name: 'Marcin', surname: 'Nowak', passwordHash: pwdHash,email:'m.nowak@example.com',spec:'Ortopeda',phone:'456456456',description:lorem,role:'Doctor',picture:'https://clinicsapp.blob.core.windows.net/pictures/doctors/doctor4.jpg', clinics:{connect:[{id: clinicsIds[3].id},{id: clinicsIds[4].id}]}},
    });
    await prisma.doctor.create({
        data: 
        { name: 'Michał', surname: 'Potrzeba', passwordHash: pwdHash,email:'m.potrzeba@example.com',spec:'Kardiolog',phone:'567567567',description:lorem,role:'Doctor',picture:'https://clinicsapp.blob.core.windows.net/pictures/doctors/doctor5.jpg', clinics:{connect:[{id: clinicsIds[4].id}]}},
    });
    await prisma.doctor.create({
        data: 
        { name: 'Franciszek', surname: 'Wiśniewski', passwordHash: pwdHash,email:'f.wisniewski@example.com',spec:'Ortopeda',phone:'678678678',description:lorem,role:'Doctor',picture:'https://clinicsapp.blob.core.windows.net/pictures/doctors/doctor6.jpg', clinics:{connect:[{id: clinicsIds[1].id}]}},
    });
    await prisma.doctor.create({
        data: 
        { name: 'Jan', surname: 'Poprawa', passwordHash: pwdHash,email:'j.poprawa@example.com',spec:'Laryngolog',phone:'789789789',description:lorem,role:'Doctor',picture:'https://clinicsapp.blob.core.windows.net/pictures/doctors/doctor7.jpg', clinics:{connect:[{id: clinicsIds[3].id},{id: clinicsIds[4].id}]}},
    });
    await prisma.doctor.create({
        data: 
        { name: 'Krzysztof', surname: 'Luta', passwordHash: pwdHash,email:'k.luta@example.com',spec:'Internista',phone:'890890890',description:lorem,role:'Doctor',picture:'https://clinicsapp.blob.core.windows.net/pictures/doctors/doctor8.jpg', clinics:{connect:[{id: clinicsIds[0].id}]}},
    });
    await prisma.doctor.create({
        data: 
        { name: 'Wojciech', surname: 'Śruba', passwordHash: pwdHash,email:'w.sruba@example.com',spec:'Internista',phone:'901901901',description:lorem,role:'Doctor',picture:'https://clinicsapp.blob.core.windows.net/pictures/doctors/doctor9.jpg', clinics:{connect:[{id: clinicsIds[1].id}]}},
    });
    await prisma.doctor.create({
        data: 
        { name: 'Tytus', surname: 'Bomba', passwordHash: pwdHash,email:'t.bomba@example.com',spec:'Psychiatra',phone:'012012012',description:lorem,role:'Doctor',picture:'https://clinicsapp.blob.core.windows.net/pictures/doctors/doctor10.jpg', clinics:{connect:[{id: clinicsIds[1].id}]}},
    });

  } 
  await createDoctor();

  const pwdHash = hashPwd('password123');
  const createPatient = await prisma.patient.createMany({
    data: [
      { name: 'Andrzej', surname: 'Duda', passwordHash: pwdHash,email:'a.duda@example.com',phone:'098098098',birthYear:1998,isPaid:true,role:'Patient' },
      { name: 'Grzegorz', surname: 'Wagon', passwordHash: pwdHash,email:'g.wagon@example.com',phone:'987987987',birthYear:1974,isPaid:true,role:'Patient' },
      { name: 'Sebastian', surname: 'Szprot', passwordHash: pwdHash,email:'s.szprot@example.com',phone:'876876876',birthYear:1982,isPaid:true,role:'Patient' },

    ],
  });



  //await prisma.doctor.createMany({
  //  data: [
  //    { name: 'Andrzej', surname: 'Wawrzyniak', password: 'password123',email:'a.wawrzyniak@example.com',spec:'Kardiolog',phone:'123123123' },
  //    { name: 'Bartosz', surname: 'Kutrzeba', password: 'password123',email:'b.kutrzeba@example.com',spec:'Internista',phone:'234234234' },
  //    { name: 'Marcin', surname: 'Kowalski', password: 'password123',email:'m.kowalski@example.com',spec:'Internista',phone:'3456456456' },
  //    { name: 'Marcin', surname: 'Nowak', password: 'password123',email:'m.nowak@example.com',spec:'Ortopeda',phone:'456456456' },
  //    { name: 'Michał', surname: 'Potrzeba', password: 'password123',email:'m.potrzeba@example.com',spec:'Kardiolog',phone:'567567567' },
  //    { name: 'Franciszek', surname: 'Wiśniewski', password: 'password123',email:'f.wisniewski@example.com',spec:'Ortopeda',phone:'678678678' },
  //    { name: 'Jan', surname: 'Poprawa', password: 'password123',email:'f.wisniewski@example.com',spec:'Laryngolog',phone:'789789789' },
  //    { name: 'Krzysztof', surname: 'Luta', password: 'password123',email:'k.luta@example.com',spec:'Internista',phone:'890890890' },
  //    { name: 'Wojciech', surname: 'Śruba', password: 'password123',email:'w.sruba@example.com',spec:'Internista',phone:'901901901' },
  //    { name: 'Tytus', surname: 'Bomba', password: 'password123',email:'t.bomba@example.com',spec:'Psychiatra',phone:'012012012' },
  //  ],
  //});
//
  





}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
