import { GetClinicResponseDto } from "src/clinics/dto/get-clinic-response.dto";
import { GetVisitResponseDto } from "src/visits/dto/get-visit-response.dto";

export class GetDoctorResponseDto {
    id: string;
    email: string;
    name: string;
    surname: string;
    spec: string;
    phone: string;
    picture?:string;
    clinics?: GetClinicResponseDto[];
    visits?:GetVisitResponseDto[];


}