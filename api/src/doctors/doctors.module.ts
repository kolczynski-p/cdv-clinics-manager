import { forwardRef, Module } from '@nestjs/common';
import { DoctorsService } from './doctors.service';
import { DoctorsController } from './doctors.controller';
import { VisitsService } from 'src/visits/visits.service';
import { ClinicsService } from 'src/clinics/clinics.service';
import { VisitsModule } from 'src/visits/visits.module';
import { ClinicsModule } from 'src/clinics/clinics.module';

@Module({
  exports:[DoctorsService],
  imports:[forwardRef(() => VisitsModule), forwardRef(() => ClinicsModule)],
  controllers: [DoctorsController],
  providers: [DoctorsService]
})
export class DoctorsModule {}
