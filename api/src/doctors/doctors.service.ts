import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { ClinicsService } from 'src/clinics/clinics.service';
import { GetClinicResponseDto } from 'src/clinics/dto/get-clinic-response.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { GetVisitResponseDto } from 'src/visits/dto/get-visit-response.dto';
import { VisitsService } from 'src/visits/visits.service';
import { CreateDoctorDto } from './dto/create-doctor.dto';
import { GetDoctorResponseDto } from './dto/get-doctor-response.dto';
import { UpdateDoctorDto } from './dto/update-doctor.dto';

@Injectable()
export class DoctorsService {
  constructor(private db: PrismaService,
    @Inject(forwardRef(() => VisitsService))
    private readonly visitsService: VisitsService,
    @Inject(forwardRef(() => ClinicsService))
    private readonly clinicsService: ClinicsService
    ) {}

  //async create(createDoctorDto: CreateDoctorDto) {
  //  return 'This action adds a new doctor';
  //}

  async findAll(options?: any): Promise<GetDoctorResponseDto[]> {
    
    const result = await this.db.doctor.findMany(options);
    console.log(result);
    return result;
  }

  async findVisits(id: string): Promise<GetDoctorResponseDto> {
    const options = {
      where: {
        id:id
      },
      include:{
        visits:true
      }
    };
    return await this.db.doctor.findFirst(options);
  }

  async findClinics( id: string): Promise<GetDoctorResponseDto> {
    const options = {
      where: {
        id:id
      },
      include:{
        clinics:true
      }
    };
    return await this.db.doctor.findFirst(options);
  }


  

  async findOne(id: string): Promise<GetDoctorResponseDto> {
    return await this.db.doctor.findUnique({ where: { id: id } });
  }

  //update(id: string, updateDoctorDto: UpdateDoctorDto) {
  //  return `This action updates a #${id} doctor`;
  //}
  //
  //remove(id: number) {
  //  return `This action removes a #${id} doctor`;
  //}
}
