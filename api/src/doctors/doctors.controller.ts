import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  forwardRef,
  Inject,
} from '@nestjs/common';
import { ClinicsService } from 'src/clinics/clinics.service';
import { GetClinicResponseDto } from 'src/clinics/dto/get-clinic-response.dto';
import { GetVisitResponseDto } from 'src/visits/dto/get-visit-response.dto';
import { VisitsService } from 'src/visits/visits.service';
import { DoctorsService } from './doctors.service';
import { CreateDoctorDto } from './dto/create-doctor.dto';
import { GetDoctorResponseDto } from './dto/get-doctor-response.dto';
import { UpdateDoctorDto } from './dto/update-doctor.dto';

@Controller('doctors')
export class DoctorsController {
  constructor(
    private readonly doctorsService: DoctorsService,
  ) {}

  //@Post()
  //create(@Body() createDoctorDto: CreateDoctorDto) {
  //  return this.doctorsService.create(createDoctorDto);
  //}

  @Get()
  async findAll(): Promise<GetDoctorResponseDto[]> {
    return await this.doctorsService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<GetDoctorResponseDto> {
    return await this.doctorsService.findOne(id);
  }

  @Get('/visits/:id')
  async findAllVisits(@Param('id') id: string): Promise<GetDoctorResponseDto> {
    return await this.doctorsService.findVisits(id);
  }

  @Get('/clinics/:id')
  async findClinics(@Param('id') id: string): Promise<GetDoctorResponseDto> {

    return await this.doctorsService.findClinics(id);
  }

  //@Patch(':id')
  //update(@Param('id') id: string, @Body() updateDoctorDto: UpdateDoctorDto) {
  //  return this.doctorsService.update(id, updateDoctorDto);
  //}
  //
  //@Delete(':id')
  //remove(@Param('id') id: string) {
  //  return this.doctorsService.remove(+id);
  //}
}
