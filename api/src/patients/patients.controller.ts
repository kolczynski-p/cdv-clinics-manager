import { Controller, Get, Post, Body, Patch, Param, Delete, Inject, forwardRef, UseGuards } from '@nestjs/common';
import { PatientsService } from './patients.service';
import { CreatePatientDto } from './dto/create-patient.dto';
import { UpdatePatientDto } from './dto/update-patient.dto';
import { VisitsService } from 'src/visits/visits.service';
import { GetVisitResponseDto } from 'src/visits/dto/get-visit-response.dto';
import { GetPatientResponseDto } from './dto/get-patient-response.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('patients')
export class PatientsController {
  constructor(private readonly patientsService: PatientsService) {}

  //@Post()
  //create(@Body() createPatientDto: CreatePatientDto) {
  //  return this.patientsService.create(createPatientDto);
  //}

  @Get()
  async findAll():Promise<GetPatientResponseDto[]> {
    return await this.patientsService.findAll();
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('/visits/:id')
  async findAllVisits(@Param('id') id: string):Promise<GetPatientResponseDto> {
    
    return await this.patientsService.findVisits(id);
  }

  @Get(':id')
  async findOne(@Param('id') id: string) :Promise<GetPatientResponseDto>{
   
    return await this.patientsService.findOne(id);
  }

  //@Patch(':id')
  //update(@Param('id') id: string, @Body() updatePatientDto: UpdatePatientDto) {
  //  return this.patientsService.update(+id, updatePatientDto);
  //}
//
  //@Delete(':id')
  //remove(@Param('id') id: string) {
  //  return this.patientsService.remove(+id);
  //}
}
