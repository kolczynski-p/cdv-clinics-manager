import { forwardRef, Module } from '@nestjs/common';
import { PatientsService } from './patients.service';
import { PatientsController } from './patients.controller';
import { VisitsService } from 'src/visits/visits.service';
import { ClinicsService } from 'src/clinics/clinics.service';
import { DoctorsService } from 'src/doctors/doctors.service';
import { VisitsModule } from 'src/visits/visits.module';
import { ClinicsModule } from 'src/clinics/clinics.module';
import { DoctorsModule } from 'src/doctors/doctors.module';

@Module({
  exports:[PatientsService],
  imports: [forwardRef(() => VisitsModule)],
  controllers: [PatientsController],
  providers: [PatientsService]
})
export class PatientsModule {}
