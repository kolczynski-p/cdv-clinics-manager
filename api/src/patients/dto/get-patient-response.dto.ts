export class GetPatientResponseDto{
id:        string;
  email     :string;
  passwordHash  :string;
  name      :string;
  surname   :string;
  picture?   :Buffer;
  phone     :string;
  birthYear :number;
  isPaid?   :boolean;
  createdAt :Date;
}