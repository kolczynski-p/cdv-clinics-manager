import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { GetVisitResponseDto } from 'src/visits/dto/get-visit-response.dto';
import { VisitsService } from 'src/visits/visits.service';
import { urlToHttpOptions } from 'url';
import { CreatePatientDto } from './dto/create-patient.dto';
import { GetPatientResponseDto } from './dto/get-patient-response.dto';
import { UpdatePatientDto } from './dto/update-patient.dto';

@Injectable()
export class PatientsService {
constructor(private readonly db:PrismaService,
  @Inject(forwardRef(() => VisitsService)) private readonly visitsService:VisitsService){

}

  //create(createPatientDto: CreatePatientDto) {
  //  return 'This action adds a new patient';
  //}

  async findAll(options?:any) : Promise<GetPatientResponseDto[]>{
    return await this.db.patient.findMany(options);
  }
  async findVisits(id:string) : Promise<GetPatientResponseDto>{
    const options = {
      where: {
        id:id
      },
      include:{
        visits:true
      }
    };
    return await this.db.patient.findUnique(options);

  }

  async findOne(id: string):Promise<GetPatientResponseDto> {
    return await this.db.patient.findUnique({where: {id: id}});
  }

  //update(id: number, updatePatientDto: UpdatePatientDto) {
  //  return `This action updates a #${id} patient`;
  //}
//
  //remove(id: number) {
  //  return `This action removes a #${id} patient`;
  //}
}
