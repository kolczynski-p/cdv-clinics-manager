import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder, SwaggerDocumentOptions } from '@nestjs/swagger';
import Helmet from 'helmet';
import cookieParser from 'cookie-parser'

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(Helmet());
  app.use(cookieParser());
  app.enableCors({
    origin: ['localhost','*'],
    credentials: true,
    
});
  

  //openApi - swagger documentation config
  const config = new DocumentBuilder()
    .setTitle('Medical visit, clinics Manager')
    .setDescription('API description')
    .setVersion('1.0')
    .addTag('clinics')
    .build();
    
    const options: SwaggerDocumentOptions =  {
      operationIdFactory: (
        controllerKey: string,
        methodKey: string
      ) => methodKey
    };
    const document = SwaggerModule.createDocument(app, config, options);
    
  SwaggerModule.setup('api', app, document);

  //app start
  await app.listen(3000);
}
bootstrap();
